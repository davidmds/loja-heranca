/*
****************************************************************
*
* The "CD" Class inherited from the Produto
*
****************************************************************
*/
#ifndef CD_H
#define CD_H

#include <iostream>
#include <fstream>
#include <string>
#include "produto.h"

using namespace std;

class CD: public Produto{
private:
  unsigned faixas;

public:
  inline CD(): Produto(), faixas(0) {}

  ostream &imprimir(ostream &out) const;
  istream &digitar(istream &in);
  istream &ler(istream &in);
  inline ostream &salvar(ostream &out){ return imprimir(out); }
};
inline ostream &operator<<(ostream &out, CD &C) {return C.imprimir(out);}
inline istream &operator>>(istream &in, CD &C) {return C.digitar(in);}

#endif
