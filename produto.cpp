/*
****************************************************************
*
* The "Produto" Base Class
*
****************************************************************
*/
#ifndef PRODUTO_CPP
#define PRODUTO_CPP

#include <iostream>
#include <fstream>
#include <string>
#include <limits>
#include <iomanip>
#include "produto.h"

using namespace std;

ostream &Produto::imprimir(ostream &out) const{
  out << fixed << setprecision(2) << '"' << nome << '"' << ";$"  << (double)(preco) / 100;

  return out;
}
istream &Produto::digitar(istream &in){
  double prov;
  cout << "Insira o nome: ";
  getline(in, nome, '\n');
  cout << "Insira o preco: ";
  in >> prov;
  preco = (prov * 100);
  in.ignore(numeric_limits<streamsize>::max(),'\n');

  return in;
}
istream &Produto::ler(istream &in){
  double prov;
  // remove "
  in.ignore(1,'"');
  //ler at� achar o "
  getline(in, nome, '"');
  //remove o ; e o $
  in.ignore(2,'$');
  in >> prov;
  preco = (prov * 100);

  return in;
}

#endif
