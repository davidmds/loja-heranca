/*
****************************************************************
*
* The "ListaLivro" Class
*
****************************************************************
*/
#ifndef LISTALIVRO_H
#define LISTALIVRO_H

#include <iostream>
#include <fstream>
#include <string>
#include "livro.h"

using namespace std;

class ListaLivro{
private:
  unsigned n;
  Livro *itens;

  void criar(unsigned tamanho);
  void copiar(const ListaLivro &X);
  void limpar();

public:
  inline ListaLivro(): n(0) {itens=NULL;} //Default
  inline ListaLivro(unsigned tamanho) {criar(tamanho);}  // Específico
  inline ListaLivro(const ListaLivro &X) {copiar(X);}  // Copia
  inline ~ListaLivro() {limpar();} // Destrutor

  void incluir(const Livro &X);
  void excluir(unsigned id);
  ostream &imprimir(ostream &out) const;
  istream &ler(istream &in);
  ostream &salvar(ostream &out) const;
};
inline ostream &operator<<(ostream &out, ListaLivro &X) {return X.imprimir(out);}

#endif
