/*
****************************************************************
*
* The "Produto" Base Class
*
****************************************************************
*/
#ifndef PRODUTO_H
#define PRODUTO_H

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Produto{
private:
  string nome;
  int preco;

public:
  inline Produto(): nome(), preco(0) {}

  ostream &imprimir(ostream &out) const;
  istream &digitar(istream &in);
  istream &ler(istream &in);
  inline ostream &salvar(ostream &out) const{ return imprimir(out); }
};
inline ostream &operator<<(ostream &out, Produto &P) {return P.imprimir(out);}
inline istream &operator>>(istream &in, Produto &P) {return P.digitar(in);}

#endif
