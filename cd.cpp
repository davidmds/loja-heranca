/*
****************************************************************
*
* The "CD" Class inherited from the Produto
*
****************************************************************
*/
#ifndef CD_CPP
#define CD_CPP

#include <iostream>
#include <fstream>
#include <string>
#include <limits>
#include <stdlib.h>
#include "cd.h"

using namespace std;

ostream &CD::imprimir(ostream &out) const{
  out << 'C' << ":" << ' ';
  Produto::imprimir(out);
  out << ";" << faixas;

  return out;
}
istream &CD::digitar(istream &in){
  in.ignore(numeric_limits<streamsize>::max(),'\n');
  Produto::digitar(in);
  cout << "Insira o numero de faixas: ";
  in >> faixas;

  return in;
}
istream &CD::ler(istream &in){
  string sigla;

  getline(in, sigla, ':');
  if(sigla != "C"){
    cerr << "Erro de sintaxe no arquivo. Nao eh um cd." << endl;
    exit(1);
    //return in;
  }
  in.ignore(1,' ');
  Produto::ler(in);

  // remove ;
  in.ignore(1,';');
  //ler inteiro
  in >> faixas;

  return in;
}

#endif
