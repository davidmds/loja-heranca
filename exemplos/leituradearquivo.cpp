#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(void){
    string A,Nome;
    int Idade, Mes;

    cout << "Digite o nome do arquivo (termine com ENTER): ";
    getline(cin, A, '\n');

    //Abre a ifstream
    ifstream meu_arq(A.c_str());

    meu_arq >> Idade;
    // remove ; e "
    meu_arq.ignore(numeric_limits<streamsize>::max(,'"');
    //ler at� achar o "
    getline(meu_arq, Nome, '"');
    //remove o ;
    meu_arq.ignore(numeric_limits<streamsize>::max(,';');
    meu_arq >> Mes;
    //remove o enter o final da linha
    meu_arq.ignore(numeric_limits<streamsize>::max(,'\n');

    cout << Nome << ';' << Idade << ';' << Mes << endl;

    //fecha a ifstream
    meu_arq.close();

    return 0;
}
