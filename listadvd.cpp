/*
****************************************************************
*
* The "ListaDVD" Class
*
****************************************************************
*/
#ifndef LISTADVD_CPP
#define LISTADVD_CPP

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include "listadvd.h"

using namespace std;

void ListaDVD::criar(unsigned tamanho){
  itens = new DVD[tamanho];
  for (unsigned i=0; i<tamanho; i++){
      itens[i] = DVD();
  }
  n = tamanho;
}
void ListaDVD::copiar(const ListaDVD &X){
  criar(X.n);
  for (unsigned i=0; i<n; i++) itens[i] = X.itens[i];
}
void ListaDVD::limpar(){
  if (itens!=NULL){
      delete itens;
  }
  n = 0;
  itens = NULL;
}

void ListaDVD::incluir(const DVD &X){
  ListaDVD prov(n+1);

  for (unsigned i=0; i<n; i++) prov.itens[i] = itens[i];
  prov.itens[n] = X;
  limpar();
  copiar(prov);
}
void ListaDVD::excluir(unsigned id){
  unsigned j = 0;
  // id come�a de 1
  if(id != 0 && id <= n){
    if(n == 1){
      limpar();
    } else {
      ListaDVD prov(n-1);
      for (unsigned i=0; i<n; i++){
        if(id-1 == i) continue;
        prov.itens[j] = itens[i];
        j++;
      }
      limpar();
      copiar(prov);
    }
  } else {
    cerr << "O item " << id << " nao existe\n";
  }
}
ostream &ListaDVD::imprimir(ostream &out) const{
  if (n==0){
      cerr << "Lista sem itens\n";
      return out;
  }
  out << "LISTADVD " << n << endl;
  for (unsigned i=0; i<n; i++) out << i+1 << " - " << itens[i] << endl;
  return out;
}
istream &ListaDVD::ler(istream &in){
  limpar();
  DVD prov;
  string nome_lista;
  unsigned tam_lista;

  getline(in, nome_lista, ' ');
  if(nome_lista != "LISTADVD"){
    cerr << "Erro de sintaxe no arquivo. Nao eh uma lista de dvds." << endl;
    exit(1);
    //return in;
  }
  in >> tam_lista;
  in.ignore(1,'\n');
  for(unsigned i=0; i < tam_lista; i++){
    prov.ler(in);
    incluir(prov);
    in.ignore(1,'\n');
  }

  return in;
}
ostream &ListaDVD::salvar(ostream &out) const{
  if (n==0){
      cerr << "Lista sem itens\n";
      return out;
  }
  out << "LISTADVD " << n << endl;
  for (unsigned i=0; i<n; i++) out << itens[i] << endl;
  return out;
}

#endif

