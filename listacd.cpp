/*
****************************************************************
*
* The "ListaCD" Class
*
****************************************************************
*/
#ifndef LISTACD_CPP
#define LISTACD_CPP

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include "listacd.h"

using namespace std;

void ListaCD::criar(unsigned tamanho){
  itens = new CD[tamanho];
  for (unsigned i=0; i<tamanho; i++){
      itens[i] = CD();
  }
  n = tamanho;
}
void ListaCD::copiar(const ListaCD &X){
  criar(X.n);
  for (unsigned i=0; i<n; i++) itens[i] = X.itens[i];
}
void ListaCD::limpar(){
  if (itens!=NULL){
      delete itens;
  }
  n = 0;
  itens = NULL;
}

void ListaCD::incluir(const CD &X){
  ListaCD prov(n+1);

  for (unsigned i=0; i<n; i++) prov.itens[i] = itens[i];
  prov.itens[n] = X;
  limpar();
  copiar(prov);
}
void ListaCD::excluir(unsigned id){
  unsigned j = 0;
  // id come�a de 1
  if(id != 0 && id <= n){
    ListaCD prov(n-1);
    for (unsigned i=0; i<n; i++){
      if(id-1 == i) continue;
      prov.itens[j] = itens[i];
      j++;
    }
    limpar();
    copiar(prov);
  } else {
    cerr << "O item " << id << " nao existe\n";
  }
}
ostream &ListaCD::imprimir(ostream &out) const{
  if (n==0){
      cerr << "Lista sem itens\n";
      return out;
  }
  out << "LISTACD " << n << endl;
  for (unsigned i=0; i<n; i++) out << i+1 << " - " << itens[i] << endl;
  return out;
}
istream &ListaCD::ler(istream &in){
  CD prov;
  string nome_lista;
  unsigned tam_lista;

  getline(in, nome_lista, ' ');
  if(nome_lista != "LISTACD"){
    cerr << "Erro de sintaxe no arquivo. Nao eh uma lista de cds." << endl;
    exit(1);
    //return in;
  }
  in >> tam_lista;
  in.ignore(1,'\n');
  for(unsigned i=0; i < tam_lista; i++){
    prov.ler(in);
    incluir(prov);
    in.ignore(1,'\n');
  }

  return in;
}
ostream &ListaCD::salvar(ostream &out){
  if (n==0){
      cerr << "Lista sem itens\n";
      return out;
  }
  out << "LISTACD " << n << endl;
  for (unsigned i=0; i<n; i++) out << itens[i] << endl;
  return out;
}

#endif
