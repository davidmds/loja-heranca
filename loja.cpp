/*
****************************************************************
*
* The "Produto" Base Class inherited from the
*
****************************************************************
*/
#ifndef LOJA_CPP
#define LOJA_CPP

#include <fstream>
#include <string>
#include "loja_heranca.h"

using namespace std;

void Loja::imprimir(void){
  LL.imprimir(cout);
  LC.imprimir(cout);
  LD.imprimir(cout);
}
void Loja::ler(const char* X){
  ifstream arq_i(X);
  LL.ler(arq_i);
  LC.ler(arq_i);
  LD.ler(arq_i);
  arq_i.close();
}
void Loja::salvar(const char* X){
  ofstream arq_o(X);
  LL.salvar(arq_o);
  LC.salvar(arq_o);
  LD.salvar(arq_o);
  arq_o.close();
}

#endif
