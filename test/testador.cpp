#include <iostream>
#include <fstream>
#include <limits>
#include "../livro.cpp"
#include "../cd.cpp"
#include "../dvd.cpp"
#include "../listalivro.cpp"
#include "../listacd.cpp"
#include "../listadvd.cpp"

using namespace std;

int main(void){
  ListaCD p;
  CD l;
  unsigned quant, id;

  cout << "Lista de cds: " << endl << p << endl;
  cout << "Insira a quantidade de cds a serem lidos: ";
  cin >> quant;
  cin.ignore(1,'\n');

  for(unsigned i=0;i<quant;i++){
    cout << "Digitar cd: " << endl;
    cin >> l;
    p.incluir(l);

    cin.ignore(numeric_limits<streamsize>::max(),'\n');
  }
  cout << "Lista de cds: " << endl << p << endl;

  //Digitando as informações e salvando no arquivo
  ofstream arq_o("arquivo.txt");
  p.salvar(arq_o);
  arq_o.close();

  //Lendo arquivo e imprimindo conteúdo
  ifstream arq_i("arquivo.txt");
  p.ler(arq_i);
  cout << "Conteudo do arquivo:\n";
  cout << p;
  arq_i.close();

  cout << "Insira o id do arquivo que voce quer excluir: ";
  cin >> id;

  p.excluir(id);
  cout << p;

  return 0;
}