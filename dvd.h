/*
****************************************************************
*
* The "DVD" Class inherited from the Produto
*
****************************************************************
*/
#ifndef DVD_H
#define DVD_H

#include <iostream>
#include <fstream>
#include <string>
#include "produto.h"

using namespace std;

class DVD: public Produto{
private:
  unsigned duracao;

public:
  inline DVD(): Produto(), duracao(0) {}

  ostream &imprimir(ostream &out) const;
  istream &digitar(istream &in);
  istream &ler(istream &in);
  inline ostream &salvar(ostream &out){ return imprimir(out); }
};
inline ostream &operator<<(ostream &out, DVD &D) {return D.imprimir(out);}
inline istream &operator>>(istream &in, DVD &D) {return D.digitar(in);}

#endif
