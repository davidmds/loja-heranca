/*
****************************************************************
*
* The "Loja" Class
*
****************************************************************
*/
#ifndef LOJA_H
#define LOJA_H

#include <fstream>
#include <string>
#include "listalivro.h"
#include "listacd.h"
#include "listadvd.h"

using namespace std;

class Loja{
private:
  ListaLivro LL;
  ListaCD LC;
  ListaDVD LD;

public:
  inline Loja(): LL(), LC(), LD() {}


  inline void incluirLivro(Livro X){ LL.incluir(X);}
  inline void incluirCD(CD X){ LC.incluir(X);}
  inline void incluirDVD(DVD X){ LD.incluir(X);}
  inline void excluirLivro(unsigned id){ LL.excluir(id); }
  inline void excluirCD(unsigned id){ LC.excluir(id); }
  inline void excluirDVD(unsigned id){ LD.excluir(id); }

  void imprimir(void);
  void ler(const char* X);
  void salvar(const char* X);
};

#endif
