/*
****************************************************************
*
* The "DVD" Class inherited from the Produto
*
****************************************************************
*/
#ifndef DVD_CPP
#define DVD_CPP

#include <iostream>
#include <fstream>
#include <string>
#include <limits>
#include <stdlib.h>
#include "dvd.h"

using namespace std;

ostream &DVD::imprimir(ostream &out) const{
  out << 'D' << ":" << ' ';
  Produto::imprimir(out);
  out << ";" << duracao;

  return out;
}
istream &DVD::digitar(istream &in){
  in.ignore(numeric_limits<streamsize>::max(),'\n');
  Produto::digitar(in);
  cout << "Insira a duracao: ";
  in >> duracao;

  return in;
}
istream &DVD::ler(istream &in){
  string sigla;

  getline(in, sigla, ':');
  if(sigla != "D"){
    cerr << "Erro de sintaxe no arquivo. Nao eh um dvd." << endl;
    exit(1);
    //return in;
  }
  in.ignore(1,' ');
  Produto::ler(in);

  // remove ;
  in.ignore(1,';');
  //ler inteiro
  in >> duracao;

  return in;
}

#endif
