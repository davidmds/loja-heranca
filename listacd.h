/*
****************************************************************
*
* The "ListaCD" Class
*
****************************************************************
*/
#ifndef LISTACD_H
#define LISTACD_H

#include <iostream>
#include <fstream>
#include <string>
#include "cd.h"

using namespace std;

class ListaCD{
private:
  unsigned n;
  CD *itens;

  void criar(unsigned tamanho);
  void copiar(const ListaCD &X);
  void limpar();

public:
  inline ListaCD(): n(0) {itens=NULL;} //Default
  inline ListaCD(unsigned tamanho) {criar(tamanho);}  // Específico
  inline ListaCD(const ListaCD &X) {copiar(X);}  // Copia
  inline ~ListaCD() {limpar();} // Destrutor

  void incluir(const CD &X);
  void excluir(unsigned id);
  ostream &imprimir(ostream &out) const;
  istream &ler(istream &in);
  ostream &salvar(ostream &out) const;
};
inline ostream &operator<<(ostream &out, ListaCD &X) {return X.imprimir(out);}

#endif
