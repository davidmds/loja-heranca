/*
****************************************************************
*
* The "ListaLivro" Class
*
****************************************************************
*/
#ifndef LISTALIVRO_CPP
#define LISTALIVRO_CPP

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include "listalivro.h"

using namespace std;

void ListaLivro::criar(unsigned tamanho){
  itens = new Livro[tamanho];
  for (unsigned i=0; i<tamanho; i++){
      itens[i] = Livro();
  }
  n = tamanho;
}
void ListaLivro::copiar(const ListaLivro &X){
  criar(X.n);
  for (unsigned i=0; i<n; i++) itens[i] = X.itens[i];
}
void ListaLivro::limpar(){
  if (itens!=NULL){
      delete itens;
  }
  n = 0;
  itens = NULL;
}

void ListaLivro::incluir(const Livro &X){
  ListaLivro prov(n+1);

  for (unsigned i=0; i<n; i++) prov.itens[i] = itens[i];
  prov.itens[n] = X;
  limpar();
  copiar(prov);
}
void ListaLivro::excluir(unsigned id){
  unsigned j = 0;
  // id come�a de 1
  if(id != 0 && id <= n){
    if(n == 1){
      limpar();
    } else {
      ListaLivro prov(n-1);
      for (unsigned i=0; i<n; i++){
        if(id-1 == i) continue;
        prov.itens[j] = itens[i];
        j++;
      }
      limpar();
      copiar(prov);
    }
  } else {
    cerr << "O item " << id << " nao existe\n";
  }
}
ostream &ListaLivro::imprimir(ostream &out) const{
  if (n==0){
      cerr << "Lista sem itens\n";
      return out;
  }
  out << "LISTALIVRO " << n << endl;
  for (unsigned i=0; i<n; i++) out << i+1 << " - " << itens[i] << endl;
  return out;
}
istream &ListaLivro::ler(istream &in){
  limpar();
  Livro prov;
  string nome_lista;
  unsigned tam_lista;

  getline(in, nome_lista, ' ');
  if(nome_lista != "LISTALIVRO"){
    cerr << "Erro de sintaxe no arquivo. Nao eh uma lista de livros." << endl;
    exit(1);
    //return in;
  }
  in >> tam_lista;
  in.ignore(1,'\n');
  for(unsigned i=0; i < tam_lista; i++){
    prov.ler(in);
    incluir(prov);
    in.ignore(1,'\n');
  }

  return in;
}
ostream &ListaLivro::salvar(ostream &out) const{
  if (n==0){
      cerr << "Lista sem itens\n";
      return out;
  }
  out << "LISTALIVRO " << n << endl;
  for (unsigned i=0; i<n; i++) out << itens[i] << endl;
  return out;
}

#endif
