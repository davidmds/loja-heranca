/*
****************************************************************
*
* The "Livro" Class inherited from the Produto
*
****************************************************************
*/
#ifndef LIVRO_H
#define LIVRO_H

#include <iostream>
#include <fstream>
#include <string>
#include "produto.h"

using namespace std;

class Livro: public Produto{
private:
  string autor;

public:
  inline Livro(): Produto(), autor() {}

  ostream &imprimir(ostream &out) const;
  istream &digitar(istream &in);
  istream &ler(istream &in);
  inline ostream &salvar(ostream &out) const{ return imprimir(out); }
};
inline ostream &operator<<(ostream &out, Livro &L) {return L.imprimir(out);}
inline istream &operator>>(istream &in, Livro &L) {return L.digitar(in);}

#endif
