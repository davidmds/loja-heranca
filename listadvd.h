/*
****************************************************************
*
* The "ListaDVD" Class
*
****************************************************************
*/
#ifndef LISTADVD_H
#define LISTADVD_H

#include <iostream>
#include <fstream>
#include <string>
#include "dvd.h"

using namespace std;

class ListaDVD{
private:
  unsigned n;
  DVD *itens;

  void criar(unsigned tamanho);
  void copiar(const ListaDVD &X);
  void limpar();

public:
  inline ListaDVD(): n(0) {itens=NULL;} //Default
  inline ListaDVD(unsigned tamanho) {criar(tamanho);}  // Específico
  inline ListaDVD(const ListaDVD &X) {copiar(X);}  // Copia
  inline ~ListaDVD() {limpar();} // Destrutor

  void incluir(const DVD &X);
  void excluir(unsigned id);
  ostream &imprimir(ostream &out) const;
  istream &ler(istream &in);
  ostream &salvar(ostream &out) const;
};
inline ostream &operator<<(ostream &out, ListaDVD &X) {return X.imprimir(out);}

#endif
