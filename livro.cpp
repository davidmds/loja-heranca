/*
****************************************************************
*
* The "Livro" Class inherited from the Produto
*
****************************************************************
*/
#ifndef LIVRO_CPP
#define LIVRO_CPP

#include <iostream>
#include <fstream>
#include <string>
#include <limits>
#include <stdlib.h>
#include "livro.h"

using namespace std;

ostream &Livro::imprimir(ostream &out) const{
  out << 'L' << ":" << ' ';
  Produto::imprimir(out);
  out << ";" << '"' << autor << '"';

  return out;
}
istream &Livro::digitar(istream &in){
  in.ignore(numeric_limits<streamsize>::max(),'\n');
  Produto::digitar(in);
  cout << "Insira o autor: ";
  getline(in, autor, '\n');

  return in;
}
istream &Livro::ler(istream &in){
  string sigla;

  getline(in, sigla, ':');
  if(sigla != "L"){
    cerr << "Erro de sintaxe no arquivo. Nao eh um livro." << endl;
    exit(1);
    //return in;
  }
  in.ignore(1,' ');
  Produto::ler(in);

  // remove ;"
  in.ignore(2,'"');
  //ler at� achar o "
  getline(in, autor, '"');

  return in;
}

#endif
